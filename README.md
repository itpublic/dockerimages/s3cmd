# s3cmd

This repository builds a container image that runs the [s3cmd](https://s3tools.org/s3cmd) command.  This is useful for working with files in S3 storage services like the one provided by [AWS](https://docs.aws.amazon.com/AmazonS3/latest/userguide/Welcome.html) or [NREC](https://uh-iaas.readthedocs.io/object.html).

The container image will be available from:

```
git.app.uib.no:4567/itpublic/dockerimages/s3cmd/s3cmd:latest
```

You need to provide your own configuration file that should be made available at
the `/home/user/.s3cfg` location for the s3cmd default to pick it up.  You can
override this location with the `S3CMD_CONFIG` environment variable.

For NREC this configuration file has the following format:

```toml
[default]
access_key = <secret>
secret_key = <secret>
host_base = object.api.<region>.nrec.no
host_bucket = object.api.<region>.nrec.no
```

The same `access_key`/`secret_key` pair is used for both NREC regions that are
either `bgo` or `osl`.

This image runs as a non-root user by default which makes it suitable for
container platforms as [RAIL](https://ntk.app.uib.no/TJ0847).

## Example

Run as:

```sh
docker run -v$(pwd)/dot-s3cfg:/home/user/.s3cfg git.app.uib.no:4567/itpublic/dockerimages/s3cmd/s3cmd:latest la
```

This example mounts the configuration file to use and runs the `s3cmd la`
command which lists all the top level files from all buckets.  Note that the
source volume needs to be an absolute path.

As an alternative to mounting the configuration file you can override the configuration values like this:

```sh
docker run --env AWS_ACCESS_KEY=... --env AWS_SECRET_KEY=... git.app.uib.no:4567/itpublic/dockerimages/s3cmd/s3cmd:latest --host=object.api.bgo.nrec.no --host-bucket=object.api.bgo.nrec.no la
```

There is unfortunately no way to provide the endpoint URL through the environment, so we have to pass this information twice as arguments with the `--host` and `--host-bucket` options shown above.
