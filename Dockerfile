FROM python:3-alpine
RUN adduser -u 10000 -D user
RUN pip install s3cmd
WORKDIR /home/user
USER 10000
ENTRYPOINT [ "s3cmd" ]